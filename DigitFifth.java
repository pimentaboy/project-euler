
public class DigitFifth {
	int expoente;
	int intermedio;
	int resultado=0;
	int valor;
	int resposta;
	
	
	public DigitFifth(int power){
		expoente=power;
	}
	
	
	
	public int getResult(){
		
		for(int i=2;i<500000;i++){
			valor=i;
			while(valor>0){
				intermedio=valor%10;
				resultado+=(int) Math.pow(intermedio, this.expoente);
				valor=valor/10;
			}
			if (resultado==i){
				resposta+=resultado;
			}
			resultado=0;

		}
		return resposta;
	}
}