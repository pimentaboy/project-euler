import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;


public class MaxPathSum {
	int x=0;
	int[][] tri;
	
	
	public MaxPathSum(int tamanho,String file) throws IOException{
		tri = new int[tamanho][];
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line;
		while ((line = br.readLine()) != null) {
			//String segments[] = line.split(" ");
			String[] strArray = line.split(" ");
			int[] intArray = new int[strArray.length];
			for(int i = 0; i < strArray.length; i++) {
			    intArray[i] = Integer.parseInt(strArray[i]);
			}
			//System.out.println(Arrays.toString(intArray));
			tri[x] = intArray.clone();
			x++;
		}
		br.close();
		System.out.println(Arrays.deepToString(tri));
		
		
		
	}
	
	public int getMax(){
		for (int i = x - 2; i >= 0; i--) {
		    for (int j = 0; j <= i; j++) {
		        tri[i][j] += Math.max(tri[i+1][j], tri[i+1][j+1]);
		    }
		}
		
		return tri[0][0];
	}

}
