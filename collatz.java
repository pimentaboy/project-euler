

public class collatz {
	long max;
	long contador;
	long maxContador=0;
	long resultado;
	
	public collatz(long n){
		max=n;
		
	}
	
	public long getCollatz(long x){
		contador=1;
		while(x!=1){
			if(x%2==0){
				x=x/2;
				contador++;
			}
			else{
				x=((3*x) + 1);
				contador++;
			}
		}
		return contador;
		
	}
	
	
	public long getResultado(){
		for(long i=2;i<max;i++){
			long valor=this.getCollatz(i);
			if(valor>maxContador){
				maxContador=valor;
				resultado=i;
				
			}
			
		}
		
		
		return resultado;
	}

}
