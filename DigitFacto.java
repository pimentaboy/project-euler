
public class DigitFacto {
	
	
	int max;
	int resultado=0;
	
	
	public DigitFacto(){
		max=2540160;
	}
	
	
	public int factorial(int x){
		int y=x;
		int w=x;
		if(x==1 || x==0){
			return 1;
		}
		else{
			while(x>1){
				y=y*(w-1);
				x--;
				w--;
		}
		return y;
		}
		
	}
	
	
	public int getResultado(){
		for (int i = 10; i < max; i++) {
		    int numeroTeste = 0;
		    int numero = i;
		    while (numero > 0) {
		        int d = numero % 10;
		        numero = numero/ 10;
		        numeroTeste += factorial(d);
		    }
		 
		    if (numeroTeste == i) {
		        resultado += i;
		    }
		    
		}
		return resultado;
	}
	

}
