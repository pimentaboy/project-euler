
public class SumSquareDif {
	int soma_quadrados;
	int quadrados_soma;
	int resultado;
	
	public SumSquareDif(int valor){
		for (int i=1;i<=valor;i++){
			soma_quadrados+=Math.pow(i,2);
		}
		for(int i=1;i<=valor;i++){
			quadrados_soma+=i;
		}
		quadrados_soma=(int) Math.pow(quadrados_soma,2);
		resultado=quadrados_soma-soma_quadrados;
		
	}

	public int getRes(){
		return this.resultado;
	}
}
