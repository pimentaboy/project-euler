import java.util.*;
import java.math.*;
public class DistinctPowers {
	HashSet<BigInteger> hm = new HashSet<BigInteger>();
	ArrayList<BigInteger> valores= new ArrayList<BigInteger>();
	long baseMax;
	long expoenteMax;
	BigInteger bi1;
	BigInteger bi2;
	
	public DistinctPowers(int x, int y){
		baseMax=x;
		expoenteMax=y;
	
	}
	
	public int calcular(){
		for(int i=2;i<=this.baseMax;i++){
			for(int e=2;e<=this.expoenteMax;e++){
				bi1= new BigInteger(String.valueOf(i));
				bi2 = bi1.pow(e);
				hm.add(bi2);
			}
		
		
		}
		return hm.size();
	}
	
}
