import java.math.*;
	


public class SelfPower {
	BigInteger big= BigInteger.valueOf(1);
	BigInteger resultado=BigInteger.valueOf(0);
	
	
	public SelfPower(int quantidade){
		for(int i=1;i<=quantidade;i++){
			resultado =resultado.add( big.pow(i));
			big = big.add(BigInteger.valueOf(1));
		}
		
	}
	
	 
	public String toString(int last_dig){
		String finale=resultado.toString();
		int num=finale.length()-last_dig;
		String resposta=finale.substring(num);
		return resposta;
	}
	
}
