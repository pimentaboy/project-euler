import java.math.*;


public class FactorialSum {
	BigInteger bi;
	BigInteger b2;
	BigInteger teste3= new BigInteger("0");
	BigInteger teste4= new BigInteger("10");
	BigInteger teste5= new BigInteger("0");
	long intermedio=0;
	
	
	
	public FactorialSum(long valor){
		bi=new BigInteger("1");
		b2=new BigInteger("1");
		
		for(long i=valor ;i>0;i--){
			bi=bi.valueOf(i);
			b2=b2.multiply(bi);
		}
	}
		
	
	public BigInteger calcular(){
		
		return this.b2;
		
	}
	
	public long getValor(){
		while(!b2.equals(teste5)) {
			teste3 = b2.remainder(teste4);
			intermedio += teste3.longValue();
			b2= b2.divide(teste4);
			
		}
		return intermedio;
		
	

	}
	
}
