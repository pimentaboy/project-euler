import java.math.*;
public class Lattice {
	
	
	
	long valore=0;
	BigInteger testare=new BigInteger("1");
	BigInteger bi;
	BigInteger b2;
	
	public Lattice(long x){
		valore=x;
		
	}
	
	public BigInteger FactorialSum(long valor){
		testare=new BigInteger("1");
		bi=new BigInteger("1");
		
		
		for(long i=valor ;i>0;i--){
			bi=bi.valueOf(i);
			testare=testare.multiply(bi);
		}
		return testare;
		
		
	}
	
	public long getValore(){
		return this.valore;
	}
	
	
	public BigInteger getResult(){
		b2 =this.FactorialSum(this.getValore()).multiply(this.FactorialSum(this.getValore()));
		b2 = this.FactorialSum(this.getValore()*2).divide(b2); 
		return b2;
		
	}

}
