import java.util.ArrayList;
import java.math.*;
public class MilFibonacci {
	BigInteger b2=new BigInteger("0");
	BigInteger bi= new BigInteger("0");
	ArrayList<BigInteger> listabigs = new ArrayList<BigInteger>(); 
	int valor=1;
	public MilFibonacci(long numero){
		valor=1;
		bi=BigInteger.valueOf(numero);
		BigInteger x= new BigInteger("0");
		BigInteger y= new BigInteger("1");
		BigInteger z= new BigInteger("1");
		for(int i=0;x.toString().length()<=numero;i++){
			x = y;
            y = z;
            z=x.add(y);
            if(x.toString().length()>=numero){
            	listabigs.add(x);
            	b2=x;
            	break;
            }
            valor++;
        }
	}
	
	public BigInteger getRes(){
		return b2;
		
	}
	
	public int getInt(){
		return valor;
	}
}
