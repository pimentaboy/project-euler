import java.math.*;
public class Combinatorios {
	BigInteger resultado= new BigInteger("0");
	long valoreMax=0;
	long resultado2=0;
	
	
	public Combinatorios(long x){
		valoreMax=x;
		
	}
	
	public BigInteger Factorial(long valor){
		
		BigInteger testare=new BigInteger("1");
		BigInteger bi=new BigInteger("1");
		
		
		for(long i=valor ;i>0;i--){
			bi=bi.valueOf(i);
			testare=testare.multiply(bi);
		}
		return testare;
		
		
	}
	
	
	public long Resultado(){
		BigInteger max=BigInteger.valueOf(this.valoreMax);
		BigInteger temp;
		for (int n = 1; n <= 100; n++) {
		    for (int r = 0; r <= n; r++) {
		        //if(Factorial(n) / (Factorial(r)*Factorial(n-r)) >= max)
		        temp=this.Factorial(n).divide(this.Factorial(r).multiply(this.Factorial(n-r)));
		        int res = temp.compareTo(max);
		        System.out.println(n);
		        System.out.println(r);
		        System.out.println(res + "\n");
		        if (res==1){
		        	resultado2++;
		        }
		     }
		}
		return resultado2;
	}
	
	
	
	public BigInteger Resultado2(){
		BigInteger max=BigInteger.valueOf(this.valoreMax);
		BigInteger temp=new BigInteger("0");
		temp=this.Factorial(23).divide(this.Factorial(10).multiply(this.Factorial(23-10)));
		return temp;
	}

}
