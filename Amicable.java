
public class Amicable {
	int numteste;
	
	public Amicable(){
		numteste=0;
	}
	
	
	
	public int propDivisor(int x){
		int resposta=1;
		for(int e=2;e<=Math.sqrt(x);e++){
			if(x%e==0){
				resposta+= (e + (x/e));
			}
		}
		return resposta;
		
		
	}
	
	public int getValor(){
		for(int i=1;i<10000;i++){
			int h1=propDivisor(i);
			int h2=propDivisor(h1);
			if(i==h2 && i!=h1){
				numteste+=(i);
			}
		}
		return numteste;
	}
	
	

}
